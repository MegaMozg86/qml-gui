import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.1
import QtGraphicalEffects 1.0

Window {
    id: mainWindow
    visible: true
    width: 1080
    height: 735

    flags: Qt.FramelessWindowHint
    color: Qt.rgba(0, 0, 0, 0)


    property bool isMaximized: false
    BorderImage
    {
        id: background
        source: "images/2.9.png"

        border { left: 30; top: 30; right: 30 }
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right

        MouseArea { // move window
            anchors.fill: parent
            property variant previousPosition
            onPressed: {
                previousPosition = Qt.point(mouseX, mouseY)
            }
            onPositionChanged: {
                if (pressedButtons == Qt.LeftButton) {
                    var dx = mouseX - previousPosition.x
                    var dy = mouseY - previousPosition.y

                    mainWindow.setX(mainWindow.x + dx)
                    mainWindow.setY(mainWindow.y + dy)

                }
            }
        }
    }

    // buttons container
    Item
    {
        id: container
        width: 200
        height: 32
        y: 20
        anchors.right: parent.right


        Row
        {
            spacing: 16
            anchors.centerIn: parent

            // option button
            Item
            {
                id: optionButton
                width: 28
                height: 28

                MouseArea {
                    id: optionarea
                    anchors.fill: parent
                    hoverEnabled: true

                    onClicked: {
                        Qt.quit()

                    }

                }

                Image {
                    id: optionimg
                    anchors.bottom: parent.bottom
                    source: optionarea.containsMouse ? (optionarea.pressed ? "images/2.10-action.png" : "images/2.10-hover.png") : "images/2.10.png"

                }
            }

            // maxmize button
            Item
            {
                id: maxmizButton
                width: 28
                height: 28

                MouseArea {
                    id: maxmizarea
                    anchors.fill: parent
                    hoverEnabled: true

                    onClicked: {

                        if(mainWindow.isMaximized)
                        {
                            mainWindow.isMaximized = false
                            mainWindow.visibility = Window.Windowed
                        }
                        else
                        {
                            mainWindow.isMaximized = true
                            mainWindow.visibility = Window.Maximized
                        }

                    }

                }

                Image {
                    id: maxmizimg
                    anchors.bottom: parent.bottom
                    source: maxmizarea.containsMouse ? (maxmizarea.pressed ? "images/2.11-action.png" : "images/2.11-hover.png") : "images/2.11.png"

                }
            }

            // minimize button
            Item
            {
                id: minimizButton
                width: 28
                height: 28

                MouseArea {
                    id: minimizarea
                    anchors.fill: parent
                    hoverEnabled: true

                    onClicked: {
                        mainWindow.visibility = Window.Minimized

                    }

                }

                Image {
                    id: minimizimg
                    anchors.bottom: parent.bottom
                    source: minimizarea.containsMouse ? (minimizarea.pressed ? "images/2.12-action.png" : "images/2.12-hover.png") : "images/2.12.png"

                }
            }

            // close button
            Item
            {
                id: closeButton
                width: 28
                height: 28

                MouseArea {
                    id: clsarea
                    anchors.fill: parent
                    hoverEnabled: true

                    onClicked: {
                        Qt.quit();
                    }

                }

                Image {
                    id: closeimg
                    anchors.bottom: parent.bottom
                    source: clsarea.containsMouse ? (clsarea.pressed ? "images/2.13-action.png" : "images/2.13-hover.png") : "images/2.13.png"

                }
            }

        }
    }

    TabView {

        anchors.top: container.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        Tab { title: "Файлы"; Item{} }
        Tab { title: "Торрент" ; Item {}}
        Tab { title: "Другой" ; Item {}}


        style: tabViewStyle
    }



    property Component tabViewStyle: TabViewStyle {
        tabsMovable: true

        frame: Rectangle {
            radius: 15
            border.color: "blue"
            color: "dark blue"

            Image
            {
                id: stripe
                fillMode: Image.TileHorizontally

                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right

                source: "images/2.14.png"

                TextField {
                    anchors.margins: 20
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    style: textfieldStyle

                    implicitWidth: 500

                    Image
                    {
                        height: parent.height
                        width: parent.height
                        anchors.right: parent.right
                        id: searchimg
                        source: "images/2.16.png"
                    }


                }


            }

            DropShadow {
                    anchors.fill: stripe
                    horizontalOffset: 3
                    verticalOffset: 5
                    radius: 0.8
                    samples: 16
                    color: "#80000000"
                    source: stripe
                }
        }

        tab: Item {
            property int space: 3           // расстояние между табами

            implicitWidth: image.sourceSize.width + space
            implicitHeight: image.sourceSize.height

            BorderImage {
                id: image

                property real x_coef: 1.05  // изменение ширины активный - неактивный
                property real y_coef: 0.7   // изменение высоты активный - неактивный

                anchors.bottom: parent.bottom
                anchors.left: parent.left

                height: styleData.selected ? sourceSize.height : sourceSize.height * y_coef
                width: styleData.selected ? sourceSize.width : sourceSize.width //* x_coef - кнопки перекрываются

                source: "images/2.6.png"
                border.left: 30
                border.right: 30

            }
            Text {
                text: styleData.title
                anchors.centerIn: parent
                color: "white"
            }

        }
        leftCorner: Item { implicitWidth: 50 }
    }

    property Component textfieldStyle: TextFieldStyle {
        background: Rectangle {
            implicitWidth: parent.width

            color: "transparent"
            antialiasing: true

            BorderImage{
                id: srchback
                border.left: 30
                border.right: 30
                anchors.fill: parent

                source: "images/2.15.png"

            }

            Text
            {
                text: "Поиск"
                anchors.centerIn: parent
                color: "white"
            }

        }
    }

}
